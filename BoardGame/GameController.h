#pragma once
#include "AIAgent.h"
#include "Board.h"
#include <stdlib.h>
class GameController
{
public:
	GameController();
	~GameController();
	Board* m_board;
	AIAgent** m_players;
	int m_numberOfPlayers;
	int ShowScreen(int screenIndex);
	void GameLoop();
	void AddPlayer(AIAgent* agentToAdd);
	void RemovePlayers();
	int m_turnsToPlay;
	int m_delayBetweenAIDecisions;
	int PlayMatch(int offset);
	bool m_enableShuffle;
};

