#pragma once
#include "Board.h"
#include <iostream>
#include <conio.h>
class AIAgent
{
public:
	AIAgent(int type);
	~AIAgent();
	int m_type;
	void MakeMove(Board* board, int playerID, int playersCount);
	bool CheckIfFieldIsWinning(Board* board, int playerID, int posX, int posY);
	int CheckIfFieldIsWinningInTwoMoves(Board* board, int playerID, int posX, int posY);
	int CheckHowManyPossibleWinningPositions(Board* board, int playerID, int posX, int posY);
	int m_paramMeWinning;
	int m_paramEnemyWinning;
	int m_paramMeWinningInTwoMoves;
	int m_paramEnemyWinningInTwoMoves;
	int m_paramPossiblePlays;
};

