#pragma once
class Board
{
public:
	Board();
	~Board();
	int** m_board;
	// Creates board of size x,y, where x,y > 2 && xy < 11
	bool CreateBoard(int sizex, int sizey);
	// Clears board of any signs
	void ClearBoard();
	// Tries to capture field for given player. Returns false is field is already occupied by other player
	bool RegisterMove(int posX, int posY, int playerID);
	// Checks whole board to see if someone won. Returns ID of player, 0 if noone won
	int CheckForWinner();
	int m_sizex;
	int m_sizey;
	void DeleteBoard();
	bool CheckForEmptyFields();
};

