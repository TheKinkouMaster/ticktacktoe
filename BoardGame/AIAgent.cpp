#include "AIAgent.h"

AIAgent::AIAgent(int type)
{
	m_type = type;
	switch (type)
	{
	case 2://random
		m_paramMeWinning = 0;
		m_paramEnemyWinning = 0;
		m_paramMeWinningInTwoMoves = 0;
		m_paramEnemyWinningInTwoMoves = 0;
		m_paramPossiblePlays = 0;
		break;
	case 3://deff
		m_paramMeWinning = 100;
		m_paramEnemyWinning = 2;
		m_paramMeWinningInTwoMoves = 10;
		m_paramEnemyWinningInTwoMoves = 1;
		m_paramPossiblePlays = 5;
		break;
	case 4://off
		m_paramMeWinning = 2;
		m_paramEnemyWinning = 100;
		m_paramMeWinningInTwoMoves = 1;
		m_paramEnemyWinningInTwoMoves = 5;
		m_paramPossiblePlays = 1;
		break;
	case 5://balance
		m_paramMeWinning = 100;
		m_paramEnemyWinning = 50;
		m_paramMeWinningInTwoMoves = 3;
		m_paramEnemyWinningInTwoMoves = 3;
		m_paramPossiblePlays = 2;
		break;
	case 6://hard
		m_paramMeWinning = 100;
		m_paramEnemyWinning = 50;
		m_paramMeWinningInTwoMoves = 10;
		m_paramEnemyWinningInTwoMoves = 2;
		m_paramPossiblePlays = 2;
		break;
	case 7://very hard
		m_paramMeWinning = 100;
		m_paramEnemyWinning = 50;
		m_paramMeWinningInTwoMoves = 2;
		m_paramEnemyWinningInTwoMoves = 4;
		m_paramPossiblePlays = 2;
		break;
	case 8://experimental
		m_paramMeWinning = 100;
		m_paramEnemyWinning = 50;
		m_paramMeWinningInTwoMoves = 1;
		m_paramEnemyWinningInTwoMoves = 1;
		m_paramPossiblePlays = 1;
		break;
	default:
		m_paramMeWinning = 0;
		m_paramEnemyWinning = 0;
		m_paramMeWinningInTwoMoves = 0;
		m_paramEnemyWinningInTwoMoves = 0;
		m_paramPossiblePlays = 0;
		break;
	}
}


AIAgent::~AIAgent()
{
}


void AIAgent::MakeMove(Board* board, int playerID, int playersCount)
{
	int moveX, moveY;
	moveX = 0;
	moveY = 0;
	if (m_type == 1)
	{
		std::cout << "Click numbers representing column and row of desire move" << std::endl;
		moveX = _getch() - 48;
		moveY = _getch() - 48;
		while (!board->RegisterMove(moveX,moveY,playerID))
		{
			std::cout << "Try again. Click numbers representing column and row of desire move" << std::endl;
			moveX = _getch() - 48;
			moveY = _getch() - 48;
		}
	}
	else
	{
		int** boardValues = new int*[board->m_sizex];
		for (int i = 0; i < board->m_sizex; i++) boardValues[i] = new int[board->m_sizey];
		
		//evaluation
		for (int i = 0; i < board->m_sizex; i++)
		{
			for (int j = 0; j < board->m_sizey; j++)
			{
				//avaiable fields are 1, non avaiable 0
				if (board->m_board[i][j] == 0)
				{
					boardValues[i][j] = 1;
					//now multiply everything but ai parameters to get set of best positions
					if (m_type != 2)
					{
						//todo am I winning?
						if (CheckIfFieldIsWinning(board, playerID, i, j))
						{
							boardValues[i][j] *= m_paramMeWinning;
						}

						//is enemy winning?


						for (int id = 1; id < playersCount + 1; id++)
						{
							if (id != playerID)
							{
								if (CheckIfFieldIsWinning(board, id, i, j))
								{
									boardValues[i][j] *= m_paramEnemyWinning;
								}
							}
						}

						//is that field 2 moves from winning?
						int multiplier = CheckIfFieldIsWinningInTwoMoves(board, playerID, i, j);
						if (multiplier > 0)
						{
							boardValues[i][j] *= multiplier * m_paramMeWinningInTwoMoves;
						}

						//is that field 2 moves from enemy winning?
						for (int id = 1; id < playersCount + 1; id++)
						{
							if (id != playerID)
							{
								multiplier = CheckIfFieldIsWinningInTwoMoves(board, id, i, j);
								if (multiplier > 0)
								{
									boardValues[i][j] *= multiplier * m_paramEnemyWinningInTwoMoves;
								}
							}
						}

						//is that field a good move for the future?
						multiplier = CheckHowManyPossibleWinningPositions(board, playerID, i, j);
						if (multiplier > 0)
						{
							boardValues[i][j] *= multiplier * m_paramPossiblePlays;
						}
					}
				}
				else boardValues[i][j] = 0;
			}
		}

		//finding maxes
		int max = 0;
		int numberOfMaxes = 0;
		for (int i = 0; i < board->m_sizex; i++)
		{
			for (int j = 0; j < board->m_sizey; j++)
			{
				if (max < boardValues[i][j])
				{
					max = boardValues[i][j];
					numberOfMaxes = 1;
				}
				else if (max == boardValues[i][j])
				{
					numberOfMaxes++;
				}
			}
		}

		//picking position
		int pickedMax = rand() % numberOfMaxes;
		int maxCounter = pickedMax;
		for (int j = 0; j < board->m_sizex; j++)
		{
			for (int i = 0; i < board->m_sizey; i++)
			{
				if (max == boardValues[j][i])
				{
					if (maxCounter != 0)maxCounter--;
					if (maxCounter == 0)
					{
						board->RegisterMove(j, i, playerID);
						maxCounter--;
					}
				}
			}
		}
	}
}


bool AIAgent::CheckIfFieldIsWinning(Board* board, int playerID, int posX, int posY)
{
	//checking in field is free
	if (board->m_board[posX][posY] != 0) return false;
	//checking all line from given position

	//rows
	if (posX + 2 < board->m_sizex) if (board->m_board[posX + 2][posY] == playerID && board->m_board[posX + 1][posY] == playerID) return true;
	if (posX + 1 < board->m_sizex && posX - 1 >= 0) if (board->m_board[posX + 1][posY] == playerID && board->m_board[posX - 1][posY] == playerID) return true;
	if (posX - 2 >= 0) if (board->m_board[posX - 2][posY] == playerID && board->m_board[posX - 1][posY] == playerID) return true;
	//collumns
	if (posY + 2 < board->m_sizey) if (board->m_board[posX][posY + 2] == playerID && board->m_board[posX][posY + 1] == playerID) return true;
	if (posY + 1 < board->m_sizey && posX - 1 >= 0) if (board->m_board[posX][posY + 1] == playerID && board->m_board[posX][posY - 1] == playerID) return true;
	if (posY - 2 >= 0) if (board->m_board[posX][posY - 2] == playerID && board->m_board[posX][posY - 1] == playerID) return true;
	//slash down
	if (posX - 2 >= 0 && posY - 2 >= 0) 
		if (board->m_board[posX - 2][posY - 2] == playerID && board->m_board[posX - 1][posY - 1] == playerID) return true;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY + 1] == playerID && board->m_board[posX - 1][posY - 1] == playerID) return true;
	if (posX + 2 < board->m_sizex && posY + 2 < board->m_sizey)
		if (board->m_board[posX + 2][posY + 2] == playerID && board->m_board[posX + 1][posY + 1] == playerID) return true;
	//slash up
	if (posX - 2 >= 0 && posY + 2 < board->m_sizey)
		if (board->m_board[posX - 2][posY + 2] == playerID && board->m_board[posX - 1][posY + 1] == playerID) return true;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY - 1] == playerID && board->m_board[posX - 1][posY + 1] == playerID) return true;
	if (posX + 2 < board->m_sizex && posY - 2 >= 0)
		if (board->m_board[posX + 2][posY - 2] == playerID && board->m_board[posX + 1][posY - 1] == playerID) return true;

	return false;
}


int AIAgent::CheckIfFieldIsWinningInTwoMoves(Board* board, int playerID, int posX, int posY)
{
	//checking in field is free
	if (board->m_board[posX][posY] != 0) return 0;
	int winningMoves = 0;
	//checking all line from given position

	//rows
	if (posX + 2 < board->m_sizex) if (board->m_board[posX + 2][posY] == 0 && board->m_board[posX + 1][posY] == playerID) winningMoves++;
	if (posX + 1 < board->m_sizex && posX - 1 >= 0) if (board->m_board[posX + 1][posY] == 0 && board->m_board[posX - 1][posY] == playerID) winningMoves++;
	if (posX - 2 >= 0) if (board->m_board[posX - 2][posY] == 0 && board->m_board[posX - 1][posY] == playerID) winningMoves++;
	if (posX + 2 < board->m_sizex) if (board->m_board[posX + 2][posY] == playerID && board->m_board[posX + 1][posY] == 0) winningMoves++;
	if (posX + 1 < board->m_sizex && posX - 1 >= 0) if (board->m_board[posX + 1][posY] == playerID && board->m_board[posX - 1][posY] == 0) winningMoves++;
	if (posX - 2 >= 0) if (board->m_board[posX - 2][posY] == playerID && board->m_board[posX - 1][posY] == 0) winningMoves++;
	//collumns
	if (posY + 2 < board->m_sizey) if (board->m_board[posX][posY + 2] == 0 && board->m_board[posX][posY + 1] == playerID) winningMoves++;
	if (posY + 1 < board->m_sizey && posX - 1 >= 0) if (board->m_board[posX][posY + 1] == 0 && board->m_board[posX][posY - 1] == playerID) winningMoves++;
	if (posY - 2 >= 0) if (board->m_board[posX][posY - 2] == 0 && board->m_board[posX][posY - 1] == playerID) winningMoves++;
	if (posY + 2 < board->m_sizey) if (board->m_board[posX][posY + 2] == playerID && board->m_board[posX][posY + 1] == 0) winningMoves++;
	if (posY + 1 < board->m_sizey && posX - 1 >= 0) if (board->m_board[posX][posY + 1] == playerID && board->m_board[posX][posY - 1] == 0) winningMoves++;
	if (posY - 2 >= 0) if (board->m_board[posX][posY - 2] == playerID && board->m_board[posX][posY - 1] == 0) winningMoves++;
	//slash down
	if (posX - 2 >= 0 && posY - 2 >= 0)
		if (board->m_board[posX - 2][posY - 2] == 0 && board->m_board[posX - 1][posY - 1] == playerID) winningMoves++;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY + 1] == 0 && board->m_board[posX - 1][posY - 1] == playerID) winningMoves++;
	if (posX + 2 < board->m_sizex && posY + 2 < board->m_sizey)
		if (board->m_board[posX + 2][posY + 2] == 0 && board->m_board[posX + 1][posY + 1] == playerID) winningMoves++;
	if (posX - 2 >= 0 && posY - 2 >= 0)
		if (board->m_board[posX - 2][posY - 2] == playerID && board->m_board[posX - 1][posY - 1] == 0) winningMoves++;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY + 1] == playerID && board->m_board[posX - 1][posY - 1] == 0) winningMoves++;
	if (posX + 2 < board->m_sizex && posY + 2 < board->m_sizey)
		if (board->m_board[posX + 2][posY + 2] == playerID && board->m_board[posX + 1][posY + 1] == 0) winningMoves++;
	//slash up
	if (posX - 2 >= 0 && posY + 2 < board->m_sizey)
		if (board->m_board[posX - 2][posY + 2] == 0 && board->m_board[posX - 1][posY + 1] == playerID) winningMoves++;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY - 1] == 0 && board->m_board[posX - 1][posY + 1] == playerID) winningMoves++;
	if (posX + 2 < board->m_sizex && posY - 2 >= 0)
		if (board->m_board[posX + 2][posY - 2] == 0 && board->m_board[posX + 1][posY - 1] == playerID) winningMoves++;
	if (posX - 2 >= 0 && posY + 2 < board->m_sizey)
		if (board->m_board[posX - 2][posY + 2] == playerID && board->m_board[posX - 1][posY + 1] == 0) winningMoves++;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY - 1] == playerID && board->m_board[posX - 1][posY + 1] == 0) winningMoves++;
	if (posX + 2 < board->m_sizex && posY - 2 >= 0)
		if (board->m_board[posX + 2][posY - 2] == playerID && board->m_board[posX + 1][posY - 1] == 0) winningMoves++;

	return winningMoves;
}


int AIAgent::CheckHowManyPossibleWinningPositions(Board* board, int playerID, int posX, int posY)
{
	//checking in field is free
	if (board->m_board[posX][posY] != 0) return 0;
	int winningMoves = 0;
	//checking all line from given position

	//rows
	if (posX + 2 < board->m_sizex) if (board->m_board[posX + 2][posY] == 0 && board->m_board[posX + 1][posY] == 0) winningMoves++;
	if (posX + 1 < board->m_sizex && posX - 1 >= 0) if (board->m_board[posX + 1][posY] == 0 && board->m_board[posX - 1][posY] == 0) winningMoves++;
	if (posX - 2 >= 0) if (board->m_board[posX - 2][posY] == 0 && board->m_board[posX - 1][posY] == 0) winningMoves++;
	//collumns
	if (posY + 2 < board->m_sizey) if (board->m_board[posX][posY + 2] == 0 && board->m_board[posX][posY + 1] == 0) winningMoves++;
	if (posY + 1 < board->m_sizey && posX - 1 >= 0) if (board->m_board[posX][posY + 1] == 0 && board->m_board[posX][posY - 1] == 0) winningMoves++;
	if (posY - 2 >= 0) if (board->m_board[posX][posY - 2] == 0 && board->m_board[posX][posY - 1] == 0) winningMoves++;
	//slash down
	if (posX - 2 >= 0 && posY - 2 >= 0)
		if (board->m_board[posX - 2][posY - 2] == 0 && board->m_board[posX - 1][posY - 1] == 0) winningMoves++;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY + 1] == 0 && board->m_board[posX - 1][posY - 1] == 0) winningMoves++;
	if (posX + 2 < board->m_sizex && posY + 2 < board->m_sizey)
		if (board->m_board[posX + 2][posY + 2] == 0 && board->m_board[posX + 1][posY + 1] == 0) winningMoves++;
	//slash up
	if (posX - 2 >= 0 && posY + 2 < board->m_sizey)
		if (board->m_board[posX - 2][posY + 2] == 0 && board->m_board[posX - 1][posY + 1] == 0) winningMoves++;
	if (posX - 1 >= 0 && posY - 1 >= 0 && posX + 1 < board->m_sizex && posY + 1 < board->m_sizey)
		if (board->m_board[posX + 1][posY - 1] == 0 && board->m_board[posX - 1][posY + 1] == 0) winningMoves++;
	if (posX + 2 < board->m_sizex && posY - 2 >= 0)
		if (board->m_board[posX + 2][posY - 2] == 0 && board->m_board[posX + 1][posY - 1] == 0) winningMoves++;

	return winningMoves;
}
