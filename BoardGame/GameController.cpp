#include "GameController.h"
#include <cstdlib>
#include <iostream>
#include <conio.h>
#include <chrono>
#include <thread>
using namespace std;


GameController::GameController()
{
	m_board = nullptr;
	m_numberOfPlayers = 0;
	m_players = nullptr;
	m_turnsToPlay = 0;
	m_delayBetweenAIDecisions = 0;
}


GameController::~GameController()
{
	if (m_board != nullptr) delete m_board;
	RemovePlayers();
}


int GameController::ShowScreen(int screenIndex)
{
	system("cls");
	
	switch (screenIndex)
	{
	case 0:
		//default screen
		cout << "Welcome to board game 1.0! Please click literally anything except power switch to continue..." << endl;
		cout << "You can also hit power switch but then we'll not continue..." << endl;
		cout << endl << endl << endl << "lol have fun" << endl;
		break;
	case 1:
		cout << "Board:   ";
		if (m_board == nullptr) cout << "not initialized. Please create board before proceeding";
		else cout << "initialized. Size: " << m_board->m_sizex << "x" << m_board->m_sizey;
		cout << endl;
		cout << "Players: ";
		if (m_players == nullptr) cout << "not initialized. Please create players queue before proceeding";
		else cout << "initialized. Size: " << m_numberOfPlayers;
		cout << endl << endl;
		cout << "1. Create new board" << endl;
		cout << "2. Setup players" << endl;
		cout << "3. Start game" << endl;
		cout << "9. Exit" << endl;
		break;
	case 2:
		cout << "Please enter new board size. Possible size ranges from 3x3 to 10x10" << endl;
		break;
	case 3:
		cout << "Please enter players to participate in upcomming match. Their iniciative will be the same as input order." << endl;
		cout << "1. Hooman player - player controlled by hooman" << endl
			<< "2. UFO aftershock AI - player that acts randomly and has no idea what is he doing" << endl
			<< "3. Gothic 3 AI - player that does his job but ignores player" << endl
			<< "4. Spellforce AI - player that wants player to lose, no matter if he wins or not" << endl
			<< "5. Age of Empires 2 AI - perfectly balanced as all things should be" << endl
			<< "6. F.E.A.R. AI - nopenopenope" << endl
			<< "7. F.E.A.R. 2 AI - in fact it is same AI but with different parameters" << endl
			<< "8. F.E.A.R. 3 AI - parameters gone wrong/testing" << endl
			<< "9. Finish" << endl << endl
			<< "Current AI Order: ";
		for (int i = 0; i < m_numberOfPlayers; i++)
		{
			cout << m_players[i]->m_type << ", ";
		}
		cout << endl << "New AI: ";
			break;
	case 4:
		cout << "Please setup tournament options." << endl;
		break;
	case 5:
		for (int j = -1; j < m_board->m_sizey; j++)
		{
			if (j == -1)
			{
				cout << "+";
				for (int i = 0; i < m_board->m_sizex; i++) cout << "++" << i;
				cout << endl;
			}
			else
			{
				cout << j;
				for (int i = 0; i < m_board->m_sizex; i++) cout << "++" << m_board->m_board[i][j];
				cout << endl;
			}
			//interline
			if (j < m_board->m_sizey - 1)
			{
				cout << "|";
				for (int i = 0; i < m_board->m_sizex; i++) cout << "  |";
				cout << endl;
			}
		}
		break;
	default:
		cout << "woops look like input was invalid. Restart this application and contact local authorities" << endl;
		break;
	}

	return 0;
}


void GameController::GameLoop()
{
	int currentScreen = 0;
	int input = 0;
	int parsedValue1, parsedValue2; // value from input acquired from _getch, parsed from ascii into int.
	int *winners = nullptr;
	while (currentScreen != -1)
	{
		ShowScreen(currentScreen);
		switch (currentScreen)
		{
		case 0:
			_getch();
			currentScreen = 1;
			break;
		case 1:
			input = _getch();
			switch (input)
			{
			case 49:
				ShowScreen(2);
				cout << "x: ";
				parsedValue1 = _getch() - 48;
				ShowScreen(2);
				cout << "y: ";
				parsedValue2 = _getch() - 48;
				
				if (parsedValue1 >= 3 && parsedValue1 <= 9 && parsedValue2 >= 3 && parsedValue2 <= 9)
				{
					if (m_board == nullptr) m_board = new Board();
					m_board->CreateBoard(parsedValue1, parsedValue2);
				}
				else 
				{
					cout << endl << "wrong parameters" << endl; 
					_getch();
				}
				break;
			case 50:
				RemovePlayers();
				input = 0;
				while (input != 57 && m_numberOfPlayers < 9)
				{
					ShowScreen(3);
					input = _getch();
					if (input == 57) break;
					parsedValue1 = input - 48;
					if (parsedValue1 >= 1 && parsedValue1 <= 8)
					{
						AddPlayer(new AIAgent(parsedValue1));
					}
				}
				break;
			case 51:
				ShowScreen(4);
				cout << "How many rounds should we play?" << endl;
				cin >> m_turnsToPlay;
				cout << endl << "How long should be a delay between AI choices (in ms)" << endl;
				cin >> m_delayBetweenAIDecisions;
				cout << "Should playing order change after each game? 0-no, 1-yes" << endl;
				cin >> m_enableShuffle;
				cout << endl;
				
				winners = new int[m_numberOfPlayers];
				parsedValue2 = 0;
				for (int i = 0; i < m_numberOfPlayers; i++) winners[i] = 0;
				for (int i = 0; i < m_turnsToPlay; i++)
				{
					input = PlayMatch(i%m_numberOfPlayers);
					if(input >= 0) winners[input-1]++;
					if(input == -1) parsedValue2++;
				}

				system("cls");
				cout << "Results: " << endl;
				for (int i = 0; i < m_numberOfPlayers; i++) cout << i+1 << ". won " << winners[i] << " times" << endl;
				cout << "Draws: " << parsedValue2 << endl;
				_getch();
				break;
			case 52:
				break;
			case 57:
				currentScreen = -1;
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}
}


void GameController::AddPlayer(AIAgent* agentToAdd)
{
	AIAgent **tmp;
	if (m_players == nullptr)
	{
		m_players = new AIAgent*;
		m_players[0] = agentToAdd;
		m_numberOfPlayers++;
	}
	else
	{
		tmp = m_players;
		m_players = new AIAgent*[m_numberOfPlayers+1];
		for (int i = 0; i < m_numberOfPlayers; i++)
		{
			m_players[i] = tmp[i];
		}
		m_players[m_numberOfPlayers] = agentToAdd;
		delete tmp;
		m_numberOfPlayers++;
	}
}


void GameController::RemovePlayers()
{
	if (m_players != nullptr)
	{
		for (int i = 0; i < m_numberOfPlayers; i++)
		{
			delete m_players[i];
		}
		delete m_players;
		m_players = nullptr;
		m_numberOfPlayers = 0;
	}
}


int GameController::PlayMatch(int offset)
{
	m_board->ClearBoard();
	int winner = 69;

	while (winner == 69)
	{
		for (int i = 0; i < m_numberOfPlayers; i++)
		{
			ShowScreen(5);
			std::this_thread::sleep_for(std::chrono::milliseconds(m_delayBetweenAIDecisions));
			cout << endl << endl;
			m_players[(i + offset) % m_numberOfPlayers]->MakeMove(m_board, ((i + offset) % m_numberOfPlayers) +1, m_numberOfPlayers);
			if (m_board->CheckForWinner() != -1) break;
			if (m_board->CheckForEmptyFields() == false) break;
		}
		ShowScreen(5);
		cout << endl << endl;
		if (m_board->CheckForWinner() != -1) winner = m_board->CheckForWinner();
		else if (m_board->CheckForEmptyFields() == false) winner = -1;
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(m_delayBetweenAIDecisions));

	return winner;
}
