#include "Board.h"



Board::Board()
{
	m_sizex = 0;
	m_sizey = 0;
}


Board::~Board()
{
	DeleteBoard();
}


// Creates board of size x,y, where x,y > 2 && xy < 11
bool Board::CreateBoard(int sizex, int sizey)
{
	DeleteBoard();
	if (sizex >= 3 && sizex <= 10 && sizey >= 3 && sizey <= 10)
	{
		m_board = new int*[sizex];
		for (int i = 0; i < sizex; i++) m_board[i] = new int[sizey];
		m_sizex = sizex;
		m_sizey = sizey;
		return true;
	}
	return false;
}


// Clears board of any signs
void Board::ClearBoard()
{
	for(int i = 0; i < m_sizex; i++)
		for (int j = 0; j < m_sizey; j++)
		{
			m_board[i][j] = 0;
		}
}


// Tries to capture field for given player. Returns false is field is already occupied by other player
bool Board::RegisterMove(int posX, int posY, int playerID)
{
	if (m_board[posX][posY] != 0)
	{
		return false;
	}
	m_board[posX][posY] = playerID;
	return true;
}


// Checks whole board to see if someone won. Returns ID of player, -1 if noone won
int Board::CheckForWinner()
{
	int possiblewinner = -1;
	//collumns
	for (int i = 0; i < m_sizex - 2; i++)
		for (int j = 0; j < m_sizey; j++)
		{
			if (m_board[i][j] == m_board[i + 1][j] && m_board[i][j] == m_board[i + 2][j] && m_board[i][j] != 0) possiblewinner = m_board[i][j];
		} 
	//rows
	for (int i = 0; i < m_sizex; i++)
		for (int j = 0; j < m_sizey - 2; j++)
		{
			if (m_board[i][j] == m_board[i][j + 1] && m_board[i][j] == m_board[i][j + 2] && m_board[i][j] != 0) possiblewinner = m_board[i][j];
		}
	//slash downside
	for (int i = 0; i < m_sizex - 2; i++)
		for (int j = 0; j < m_sizey - 2; j++)
		{
			if (m_board[i][j] == m_board[i+1][j + 1] && m_board[i][j] == m_board[i+2][j + 2] && m_board[i][j] != 0) possiblewinner = m_board[i][j];
		}
	//slash upside
	for (int i = 2; i < m_sizex; i++)
		for (int j = 0; j < m_sizey - 2; j++)
		{
			if (m_board[i][j] == m_board[i - 1][j + 1] && m_board[i][j] == m_board[i - 2][j + 2] && m_board[i][j] != 0) possiblewinner = m_board[i][j];
		}
	
	return possiblewinner;
}


void Board::DeleteBoard()
{
	for (int i = 0; i < m_sizex; i++)
	{
		delete(m_board[i]);
	}
	delete(m_board);
}


bool Board::CheckForEmptyFields()
{
	for (int i = 0; i < m_sizex; i++)
		for (int j = 0; j < m_sizey; j++)
		{
			if (m_board[i][j] == 0) return true;
		}
	return false;
}
